using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;


public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        try
        {
            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (TcpClient client = new TcpClient(host, port))
            {
                try
                {
                    NetworkStream stream = client.GetStream();
                    StreamReader reader = new StreamReader(stream);
                    StreamWriter writer = new StreamWriter(stream);
                    writer.AutoFlush = true;


                    new Bot(reader, writer, new Join(botName, botKey));
                }
                catch (Exception)
                {

                }
            }
        }
        catch (Exception)
        {

        }
	}
    StreamWriter file = new StreamWriter(@"WriteLines2.txt");
	private StreamWriter writer;
    private GameInit initData=null;

	Bot(StreamReader reader, StreamWriter writer, Join join) {
		this.writer = writer;
		string line;
        double lastThrottle = 1.0;
        double lastPieceDistance = 0;
        double lastangle = 0;
		send(join);
        double speed=0;
        int lastpieceindex = 0;
        int currentLane=0;
        double maxangle=0;
		while((line = reader.ReadLine()) != null) {
            try
            {
                MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                //Data dat = JsonConvert.DeserializeObject<Data>(msg.data.ToString());
                Console.WriteLine(line);
                file.WriteLine(line);
                switch (msg.msgType)
                {
                    case "carPositions":
                        //send(new Throttle(0.5));
                        CarPosition msg1 = JsonConvert.DeserializeObject<CarPosition>(line);
                        //if (msg1.data[0].angle > 0 || msg1.data[0].angle < 0)
                        //{
                        //    send(new Throttle(0.6));
                        //}
                        //else
                        //{
                        //    send(new Throttle(1.0));
                        //}
                       

                       
                        double? angle =GetNextTrackAngle(msg1.data[0].piecePosition.pieceIndex+1);
                        double? nextangle = GetNextTrackAngle(msg1.data[0].piecePosition.pieceIndex+2);
                        double newspeed = CalcSpeed(lastPieceDistance, msg1.data[0].piecePosition.inPieceDistance);
                       
                        if (newspeed == 0)
                        {
                            newspeed = speed;
                        }
                        //if (lastpieceindex!=msg1.data[0].piecePosition.pieceIndex && msg1.data[0].piecePosition.pieceIndex==4) {
                        //    send(new SwitchLane("Right"));
                        //} else if (lastpieceindex!=msg1.data[0].piecePosition.pieceIndex && msg1.data[0].piecePosition.pieceIndex==14) {
                        //    send(new SwitchLane("Left"));
                        //} else if (lastpieceindex!=msg1.data[0].piecePosition.pieceIndex && msg1.data[0].piecePosition.pieceIndex==19) {
                        //    send(new SwitchLane("Right"));
                        //} else
                        if (msg1.data[0].angle > 53 || msg1.data[0].angle < -53)
                        {
                            lastThrottle = 0;
                        }
                        else if (newspeed < 5 && newspeed!=0)
                        {
                            lastThrottle = 1;
                        }
                        else if (Math.Abs(lastangle) - Math.Abs(msg1.data[0].angle) < -3)
                        {
                            lastThrottle = lastThrottle - 0.35; //0.25
                        }
                        else if (Math.Abs(lastangle) - Math.Abs(msg1.data[0].angle) < -5)
                        {
                            lastThrottle = lastThrottle - 0.44; //0.35
                        }
                        else if ((nextangle >= 44 || nextangle <= -44) && lastThrottle >= 0.6)
                        {
                            if (angle==null || (Math.Abs((double)angle) < 1 && newspeed < 5))
                            {
                                lastThrottle = 0.8; //0.8
                            }
                            else if (Math.Abs(lastangle) - Math.Abs(msg1.data[0].angle) > 3 && newspeed < 6)
                            {
                                lastThrottle = 0.7;
                            }  
                            else
                            {
                                if (newspeed > 6)
                                {
                                    lastThrottle = 0;
                                }
                                else
                                {
                                    if (Math.Abs(lastangle) - Math.Abs(msg1.data[0].angle) > 2)
                                    {
                                        lastThrottle = 0.9;//0.9
                                    }
                                    else
                                    {
                                        lastThrottle = 0.7;//0.7
                                    }
                                }
                            }
                        }
                        else if (Math.Abs(lastangle) - Math.Abs(msg1.data[0].angle) > 5)
                        {
                            lastThrottle = lastThrottle + 0.6; //0.45
                        }
                        else if (Math.Abs(lastangle) - Math.Abs(msg1.data[0].angle) > 3)
                        {
                            lastThrottle = lastThrottle + 0.4; //0.3
                        }
                        else if (angle == null || (angle < 13 && angle > -13))
                        {
                            lastThrottle = 1;
                        }
                        else if (angle < 10 && angle > -10)
                        {
                            lastThrottle = 1.0;
                        }
                        else if (angle < 25 && angle > -25)
                        {
                            lastThrottle = 1;
                        }
                        else
                        {
                            if (newspeed < 2)
                            {
                                lastThrottle = 1;
                            }
                            else if (newspeed < 3)
                            {
                                lastThrottle = 0.9;
                            }
                            else
                            {
                                lastThrottle = 0.7; //0.65
                            }
                        }


                        if (lastThrottle < 0)
                        {
                            lastThrottle = 0;
                        }
                        if (lastThrottle > 1)
                        {
                            lastThrottle = 1;
                        }
                        if (maxangle < msg1.data[0].angle)
                        {
                            maxangle = msg1.data[0].angle;
                        }
                        lastPieceDistance = msg1.data[0].piecePosition.inPieceDistance;
                        lastangle = msg1.data[0].angle;
                        send(new Throttle(lastThrottle));
                        file.WriteLine("throttle: " + lastThrottle.ToString() + " angle: " + angle.ToString() + " nextangle: " + nextangle.ToString() + " speed: " + newspeed.ToString());
                        //Console.WriteLine(msg1.gameId);
                        break;
                    case "join":
                        Console.WriteLine("Joined");
                        send(new Ping());
                        break;
                    case "gameInit":
                        Console.WriteLine("Race init");
                        initData = JsonConvert.DeserializeObject<GameInit>(line);
                        send(new Ping());
                        break;
                    case "gameEnd":
                        Console.WriteLine("Race ended");
                        send(new Ping());
                        break;
                    case "gameStart":
                        Console.WriteLine("Race starts");
                        send(new Ping());
                        break;
                    default:
                        send(new Ping());
                        break;
                }
                Console.WriteLine(maxangle);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
		}
	}

    private double CalcSpeed(double last, double current)
    {
        if (current < last)
        {
            return 0;
        }
        else
        {
            return current - last;
        }
    }
    private bool isInnerLane()
    {
        return false;
    }

    private double? GetNextTrackAngle(int currentIndex)
    {
        List<Piece> pieces = initData.data.race.track.pieces;
        if (currentIndex >= pieces.Count-1)
        {
            return pieces[0].angle;
        }
        else
        {
            return pieces[currentIndex+1].angle;
        }
    }

    private string Get(string data, string getAttr)
    {
        //data.Substring(data.IndexOf(getAttr)+getAttr.Length,
        return "";
    }

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public object data;

    public MsgWrapper(string msgType, object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}


class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }

    protected override object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

 class Id
{
    public string name { get; set; }
    public string color { get; set; }
}

 class LanE
{
    public int startLaneIndex { get; set; }
    public int endLaneIndex { get; set; }
}

 class PiecePosition
{
    public int pieceIndex { get; set; }
    public double inPieceDistance { get; set; }
    public LanE lane { get; set; }
    public int lap { get; set; }
}

 class Datum
{
    public Id id { get; set; }
    public double angle { get; set; }
    public PiecePosition piecePosition { get; set; }
}

 class CarPosition
{
    public string msgType { get; set; }
    public List<Datum> data { get; set; }
    public string gameId { get; set; }
    public int gameTick { get; set; }
}


 class Piece
 {
     public double length { get; set; }
     public bool? @switch { get; set; }
     public int? radius { get; set; }
     public double? angle { get; set; }
 }

  class Lane
 {
     public int distanceFromCenter { get; set; }
     public int index { get; set; }
 }

  class Position
 {
     public double x { get; set; }
     public double y { get; set; }
 }

  class StartingPoint
 {
     public Position position { get; set; }
     public double angle { get; set; }
 }

  class Track
 {
     public string id { get; set; }
     public string name { get; set; }
     public List<Piece> pieces { get; set; }
     public List<Lane> lanes { get; set; }
     public StartingPoint startingPoint { get; set; }
 }



  class Dimensions
 {
     public double length { get; set; }
     public double width { get; set; }
     public double guideFlagPosition { get; set; }
 }

  class Car
 {
     public Id id { get; set; }
     public Dimensions dimensions { get; set; }
 }

  class RaceSession
 {
     public int laps { get; set; }
     public int maxLapTimeMs { get; set; }
     public bool quickRace { get; set; }
 }

  class Race
 {
     public Track track { get; set; }
     public List<Car> cars { get; set; }
     public RaceSession raceSession { get; set; }
 }

  class Data
 {
     public Race race { get; set; }
 }

  class GameInit
 {
     public string msgType { get; set; }
     public Data data { get; set; }
     public string gameId { get; set; }
 }

